const express = require('express')
const bodyParser = require('body-parser')

// Create express instnace
const app = express()

// Require API routes
const fetch = require('./routes/fetch')
const config = {                                                                                                                                                        
        "api": "http://localhost:8000"
}
// Import API Routes
app.use(bodyParser.json())
app.use(fetch)
app.set("config",config)

// Export the server middleware
module.exports = {
  path: '/api',
  handler: app
}
