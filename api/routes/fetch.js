const { Router } = require('express')
const rp = require('request-promise')

const router = Router()

router.post('/crawler', function (req, res, next) {
  
    console.log('req.body', req.body)
    let options  = {
			uri: req.app.settings.config.api + "/scrapper",
			body: req.body,
			method: "post",
			json: true
		   }
    rp(options)
	.then(function(body){
	   console.log(body)
	   res.send(body)
    	})
	.catch(function(error){
	   console.log(error)
	   res.send([])
	})
})


module.exports = router
